﻿-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- ************************************** [Trainers]

CREATE TABLE [Trainers]
(
 [TrainerID]     int IDENTITY (1, 1) NOT NULL ,
 [TRFirstName]   nvarchar(50) NOT NULL ,
 [TRLastName]    nvarchar(50) NOT NULL ,
 [TRSpecialty]   nvarchar(50) NOT NULL ,
 [TRPhoneNumber] nvarchar(50) NOT NULL ,
 [TRPhoto]       image NULL ,
 [TREmail]       nvarchar(50) NOT NULL ,

 CONSTRAINT [PK_Trainers] PRIMARY KEY CLUSTERED ([TrainerID] ASC)
);
GO








-- ************************************** [Products]

CREATE TABLE [Products]
(
 [ProductID]       int IDENTITY (1, 1) NOT NULL ,
 [ProductName]     nvarchar(50) NOT NULL ,
 [ProductCategory] nvarchar(50) NOT NULL ,
 [ProductQuantity] int NOT NULL ,
 [ProductPrice]    smallmoney NOT NULL ,

 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([ProductID] ASC)
);
GO








-- ************************************** [Memberships]

CREATE TABLE [Memberships]
(
 [MemShipID]       int IDENTITY (1, 1) NOT NULL ,
 [MemShipType]     nvarchar(50) NOT NULL ,
 [MemShipNotes]    nvarchar(50) NULL ,
 [MemShipPrice]    smallmoney NOT NULL ,
 [MemShipDiscount] decimal(3,0) NULL ,

 CONSTRAINT [PK_Memberships] PRIMARY KEY CLUSTERED ([MemShipID] ASC)
);
GO








-- ************************************** [Employees]

CREATE TABLE [Employees]
(
 [EmpID]          int IDENTITY (1, 1) NOT NULL ,
 [EmpFirstName]   nvarchar(50) NOT NULL ,
 [EmpLastName]    nvarchar(50) NOT NULL ,
 [EmpTitle]       nvarchar(50) NOT NULL ,
 [EmpPhoneNumber] nvarchar(50) NOT NULL ,
 [EmpPhoto]       image NULL ,
 [EmpUser]        nvarchar(50) NOT NULL ,
 [EmpPassword]    nvarchar(50) NOT NULL ,
 [EmpEmail]       nvarchar(50) NOT NULL ,

 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED ([EmpID] ASC)
);
GO








-- ************************************** [Sessions]

CREATE TABLE [Sessions]
(
 [SessionsID]       int IDENTITY (1, 1) NOT NULL ,
 [TrainerID]        int NOT NULL ,
 [MemberID]         int NOT NULL ,
 [SessionType]      nvarchar(50) NOT NULL ,
 [SessionDate]      date NOT NULL ,
 [SessionStartTime] time(7) NOT NULL ,
 [SessionEndTime]   time(7) NOT NULL ,

 CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED ([SessionsID] ASC),
 CONSTRAINT [FK_112] FOREIGN KEY ([TrainerID])  REFERENCES [Trainers]([TrainerID]),
 CONSTRAINT [FK_111] FOREIGN KEY ([MemberID])  REFERENCES [Members]([MemberID])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_112] ON [Sessions] 
 (
  [TrainerID] ASC
 )

GO







-- ************************************** [Members]

CREATE TABLE [Members]
(
 [MemberID]            int IDENTITY (1, 1) NOT NULL ,
 [MemFirstName]        nvarchar(50) NOT NULL ,
 [MemLastName]         nvarchar(50) NOT NULL ,
 [MemDateofBirth]      date NOT NULL ,
 [MemShipID]           int NOT NULL ,
 [MemStatus]           nvarchar(50) NOT NULL ,
 [MemAddress]          nvarchar(50) NOT NULL ,
 [MemPhoneNumber]      nvarchar(50) NOT NULL ,
 [MemPhoto]            image NULL ,
 [MemEmail]            nvarchar(50) NOT NULL ,
 [MemRegistartionDate] date NOT NULL ,
 [MemExpiryDate]       date NOT NULL ,

 CONSTRAINT [PK_Members] PRIMARY KEY CLUSTERED ([MemberID] ASC),
 CONSTRAINT [FK_136] FOREIGN KEY ([MemShipID])  REFERENCES [Memberships]([MemShipID])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_136] ON [Members] 
 (
  [MemShipID] ASC
 )

GO







-- ************************************** [Sales]

CREATE TABLE [Sales]
(
 [SalesID]      int IDENTITY (1, 1) NOT NULL ,
 [EmpID]        int NOT NULL ,
 [MemberID]     int NOT NULL ,
 [SaleDate]     datetime NOT NULL ,
 [ProductID]    int NOT NULL ,
 [UnitPrice]    smallmoney NOT NULL ,
 [UnitQuantity] int NOT NULL ,
 [Discount]     real NULL ,

 CONSTRAINT [PK_Sales] PRIMARY KEY CLUSTERED ([SalesID] ASC),
 CONSTRAINT [FK_87] FOREIGN KEY ([MemberID])  REFERENCES [Members]([MemberID]),
 CONSTRAINT [FK_90] FOREIGN KEY ([EmpID])  REFERENCES [Employees]([EmpID]),
 CONSTRAINT [FK_93] FOREIGN KEY ([ProductID])  REFERENCES [Products]([ProductID])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_87] ON [Sales] 
 (
  [MemberID] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_90] ON [Sales] 
 (
  [EmpID] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_93] ON [Sales] 
 (
  [ProductID] ASC
 )

GO







-- ************************************** [CheckIns]

CREATE TABLE [CheckIns]
(
 [CheckInID]        int IDENTITY (1, 1) NOT NULL ,
 [MemberID]         int NOT NULL ,
 [SessionsID]       int NULL ,
 [CheckInDateTime]  datetime NOT NULL ,
 [CheckOutDateTime] datetime NULL ,

 CONSTRAINT [PK_CheckIns] PRIMARY KEY CLUSTERED ([CheckInID] ASC),
 CONSTRAINT [FK_106] FOREIGN KEY ([MemberID])  REFERENCES [Members]([MemberID]),
 CONSTRAINT [FK_121] FOREIGN KEY ([SessionsID])  REFERENCES [Sessions]([SessionsID])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_106] ON [CheckIns] 
 (
  [MemberID] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_121] ON [CheckIns] 
 (
  [SessionsID] ASC
 )

GO







