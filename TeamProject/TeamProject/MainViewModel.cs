﻿using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.Xpf.Scheduling;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
  [POCOViewModel]
   public class MainViewModel
    {


        GymContext myGymContext;
        public virtual ObservableCollection<Trainer> Resources { get; set; }
        public virtual ObservableCollection<Scheduling> Appointments { get; set; }


        public void AppointmentsUpdated()
        {
            myGymContext.SaveChanges();
        }

        public void InitNewAppointment(AppointmentItemEventArgs args)
        {
            args.Appointment.Reminders.Clear();
        }

        protected MainViewModel()
        {
            myGymContext = new GymContext();
            myGymContext.Trainers.Load();
            Resources = myGymContext.Trainers.Local;
            myGymContext.Schedulings.Load();
            Appointments = myGymContext.Schedulings.Local;
        }

        public static MainViewModel Create()
            {
                return ViewModelSource.Create(() => new MainViewModel());
            }
        }
    }


