namespace TeamProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CheckIn
    {
        public int CheckInID { get; set; }

        public int MemberID { get; set; }

        public int? SessionsID { get; set; }

        public DateTime CheckInDateTime { get; set; }

        public DateTime? CheckOutDateTime { get; set; }

        public virtual Member Member { get; set; }

        public virtual Session Session { get; set; }
    }
}
