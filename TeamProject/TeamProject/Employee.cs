namespace TeamProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Sales = new HashSet<Sale>();
        }

        [Key]
        public int EmpID { get; set; }

        [Required]
        [StringLength(50)]
        public string EmpFirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string EmpLastName { get; set; }

        [Required]
        [StringLength(50)]
        public string EmpTitle { get; set; }

        [Required]
        [StringLength(50)]
        public string EmpPhoneNumber { get; set; }

        [Column(TypeName = "image")]
        public byte[] EmpPhoto { get; set; }

        [Required]
        [StringLength(50)]
        public string EmpUser { get; set; }

        [Required]
        [StringLength(50)]
        public string EmpPassword { get; set; }

        [Required]
        [StringLength(50)]
        public string EmpEmail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sale> Sales { get; set; }
    }
}
