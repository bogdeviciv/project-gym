﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Ribbon;
using System.Data.SqlClient;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for CustomAppointmentWindow.xaml
    /// </summary>
    public partial class CustomAppointmentWindow : DXRibbonWindow
    {
        public CustomAppointmentWindow()
        {
            InitializeComponent();
        }

        private void tbMemberID_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    if (int.TryParse(tbMemberID.Text, out int memId))
                    {
                        var MemberFound = (from x in ctx.Members where x.MemberID == memId select x.MemName).ToList();
                        if (MemberFound.Count != 0)
                        {
                            tbMemberName.Text = MemberFound[0];
                        }
                        else
                        {
                            tbMemberName.Text = "No members found!";
                            tbMemberName.BorderBrush = System.Windows.Media.Brushes.Red;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
