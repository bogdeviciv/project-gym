﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for Shop2.xaml
    /// </summary>
    public partial class Shop2 : DevExpress.Xpf.Core.ThemedWindow
    {       

        public Shop2(Window parent)
        {
            this.Owner = parent;
            InitializeComponent();
            lvCart.ItemsSource = Globals.cart;        
           


            using (GymContext ctx = new GymContext())
            {
                var products = (from p in ctx.Products select p).ToList();
                lvShop.ItemsSource = products;
                
                
            }
            
        }

        private void btAddToCart_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            Product selected = b.CommandParameter as Product;
            //MessageBox.Show(selected.ProductName);
            if (selected.ProductQuantity == 0)
            {
                MessageBox.Show("Item is out of stock!");
                return;
            }
            int productID = selected.ProductID;
            string name = selected.ProductName;
            decimal price = selected.ProductPrice;
            decimal linetotal = price;          
            int quantity = 1;

            Cart addNew = new Cart()
            {
                ProductName = name,
                ProductID = productID,
                ProductPrice = price,
                Quantity = quantity,
                LineTotal = linetotal
            };

            if (Globals.cart.Any(prod => prod.ProductName == addNew.ProductName))
            {
                int index = Globals.cart.FindIndex(c => c.ProductName == addNew.ProductName);
                Globals.cart.ElementAt(index).Quantity +=1;
                Globals.cart.ElementAt(index).LineTotal = Globals.cart.ElementAt(index).Quantity * Globals.cart.ElementAt(index).ProductPrice;
                lvCart.Items.Refresh();
            }
            else {
                Globals.cart.Add(addNew);

            }
            lvCart.Items.Refresh();       
            
        }

        private void btnReceipt_Click(object sender, RoutedEventArgs e)
        {
            DateTime localDate = DateTime.Now;
            if ((string)lblTotal.Content == "$0")
            {
                MessageBox.Show("Please Finalize bill");
                return;
            }
            if(tbMemberID.Text == "")
            {
                MessageBox.Show("Please Enter MemberID");
                return;
            }
            if(Globals.jobPosition == "Admin")
            {
                MessageBox.Show("Admins are not able to make transactions,\n Please sign out and enter as Employee");
                return;
            }
            using(GymContext ctx = new GymContext())
            {
              
                foreach( var items in Globals.cart)
                {
                    Sale x = new Sale()
                    {
                        EmpID = Globals.empId,
                        MemberID = int.Parse(tbMemberID.Text),
                        SaleDate = localDate,
                        ProductID = items.ProductID,
                        UnitPrice = items.ProductPrice,
                        UnitQuantity = items.Quantity
                    };
                    var rmvQuan = (from p in ctx.Products where p.ProductID == x.ProductID select p).ToList();
                    if(rmvQuan.Count == 0)
                    {
                        Console.WriteLine("Record not found");
                    }
                    else
                    {
                        Product p = rmvQuan[0];
                        p.ProductQuantity = p.ProductQuantity - items.Quantity;
                    }
                    ctx.Sales.Add(x);
                    ctx.SaveChanges();
                }
                
                
            }
            Receipt rdc = new Receipt(this);
            rdc.ShowDialog();
        }

        private void btnFinal_Click(object sender, RoutedEventArgs e)
        {
            if (Globals.TotalCost != 0)
            {
                Globals.TotalCost = 0;
            }
            foreach (var x in Globals.cart)
            {
                Globals.TotalCost += x.LineTotal;
            }
            lblTotal.Content ="$"+Globals.TotalCost;
        }

        private void lvCart_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Cart x = lvCart.SelectedItem as Cart;
            if (Globals.cart.Any(prod => prod.ProductName == x.ProductName))
            {
                int index = Globals.cart.FindIndex(c => c.ProductName == x.ProductName);
                Globals.cart.ElementAt(index).Quantity -= 1;
                Globals.cart.ElementAt(index).LineTotal = Globals.cart.ElementAt(index).Quantity * Globals.cart.ElementAt(index).ProductPrice;                
                lvCart.Items.Refresh();

                if(Globals.cart.ElementAt(index).Quantity == 0)
                {
                    Globals.cart.Remove(x);
                    lvCart.Items.Refresh();
                }
            }
            else
            {
                Globals.cart.Remove(x);
                lvCart.Items.Refresh();
            }
           
        }
    }
    public class Cart
    {
       public string ProductName { get; set; }
        public int ProductID { get; set; }
      public  decimal ProductPrice { get; set; }
       public  int Quantity { get; set; }
       public  decimal LineTotal { get; set; }
             
    }

}
