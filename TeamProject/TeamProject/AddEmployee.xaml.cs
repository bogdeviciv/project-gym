﻿using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddEmployee : DevExpress.Xpf.Core.ThemedWindow
    {
        public ObservableCollection<FilterInfo> VideoDevices { get; set; }
        byte[] byteImage;

        public FilterInfo CurrentDevice
        {
            get { return _currentDevice; }
            set { _currentDevice = value; this.OnPropertyChanged("CurrentDevice"); }
        }
        private FilterInfo _currentDevice;
        private IVideoSource _videoSource;


        
        public AddEmployee(Window parent)
        {
            InitializeComponent();
            Owner = parent;
            this.DataContext = this;
            GetVideoDevices();          

            this.Closing += MainWindow_Closing;
        }

        
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StopCamera();
        }
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            StartCamera();
        }

        private void btnPhoto_Click(object sender, RoutedEventArgs e)
        {
            StopCamera();
            if (CurrentDevice == null)
            {
                return;
            }
            imgMem.Source = imgMem.Source.Clone();
            BitmapImage bi = (BitmapImage)imgMem.Source;
            byteImage = ConvertToByteFromBitmapImage(bi);
        }

        private void GetVideoDevices()
        {
            VideoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                VideoDevices.Add(filterInfo);
            }
            if (VideoDevices.Any())
            {
                CurrentDevice = VideoDevices[0];
            }
            else
            {
                MessageBox.Show("No video sources found", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                //Application.Current.Shutdown();
            }
        }

        private void video_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
        {
            try
            {
                BitmapImage bi;
                using (var bitmap = (Bitmap)eventArgs.Frame.Clone())
                {
                    bi = bitmap.ToBitmapImage();
                }
                bi.Freeze(); // avoid cross thread operations and prevents leaks
                Dispatcher.BeginInvoke(new ThreadStart(delegate { imgMem.Source = bi; }));
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error on _videoSource_NewFrame:\n" + exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                StopCamera();
            }
        }

        private void StartCamera()
        {
            if (CurrentDevice != null)
            {
                _videoSource = new VideoCaptureDevice(CurrentDevice.MonikerString);
                _videoSource.NewFrame += video_NewFrame;
                _videoSource.Start();
            }
        }

        private void StopCamera()
        {
            if (_videoSource != null && _videoSource.IsRunning)
            {
                _videoSource.SignalToStop();
                _videoSource.NewFrame -= new NewFrameEventHandler(video_NewFrame);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public static Byte[] ConvertToByteFromBitmapImage(BitmapImage bitmapImage)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }

        private void btSaveUser_Click(object sender, RoutedEventArgs e)
        {
            string fname = tbFirstName.Text;
            string lname = tbLastName.Text;
            string title = tbTitle.Text;
            string phone = tbPhoneNum.Text;
            string login = tbUserName.Text;
            string password = tbPassword.Text;
            string email = tbEmail.Text;
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    Employee newEmp = new Employee()
                    {
                        EmpFirstName = fname,
                        EmpLastName = lname,
                        EmpTitle = title,
                        EmpPhoneNumber = phone,
                        EmpUser = login,
                        EmpPassword = password,
                        EmpPhoto = byteImage,
                        EmpEmail = email,

                    };
                    ctx.Employees.Add(newEmp);
                    ctx.SaveChanges();
                }
                this.DialogResult = true;
            }catch (SqlException ex)
            {
                MessageBox.Show(this, "Error adding new Employee:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);                
            }

        }
    }
}
