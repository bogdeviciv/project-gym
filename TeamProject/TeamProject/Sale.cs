namespace TeamProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Sale
    {
        [Key]
        public int SalesID { get; set; }

        public int EmpID { get; set; }

        public int MemberID { get; set; }

        public DateTime SaleDate { get; set; }

        public int ProductID { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal UnitPrice { get; set; }

        public int UnitQuantity { get; set; }

        public float? Discount { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Member Member { get; set; }

        public virtual Product Product { get; set; }
    }
}
