﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    class Globals
    {
        public static string loginFName;
        public static string loginLName;
        public static string jobPosition;
        public static int memId;
        public static int empId;
        public static decimal TotalCost;

        public static List<Cart> cart = new List<Cart>();
    }
}
