namespace TeamProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Member
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Member()
        {
            CheckIns = new HashSet<CheckIn>();
            Sessions = new HashSet<Session>();
            Sales = new HashSet<Sale>();
        }

        public int MemberID { get; set; }

        

        [Required]
        [StringLength(50)]
        public string MemName { get; set; }

        [Column(TypeName = "date")]
        public DateTime MemDateofBirth { get; set; }

        public int MemShipID { get; set; }

        [Required]
        [StringLength(50)]
        public string MemStatus { get; set; }

        [Required]
        [StringLength(50)]
        public string MemAddress { get; set; }

        [Required]
        [StringLength(50)]
        public string MemPhoneNumber { get; set; }

        [Column(TypeName = "image")]
        public byte[] MemPhoto { get; set; }

        [Required]
        [StringLength(50)]
        public string MemEmail { get; set; }

        [Column(TypeName = "date")]
        public DateTime MemRegistartionDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime MemExpiryDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckIn> CheckIns { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session> Sessions { get; set; }

        public virtual Membership Membership { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sale> Sales { get; set; }
    }
}
