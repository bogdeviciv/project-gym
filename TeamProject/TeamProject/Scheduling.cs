﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    [Table("Scheduling")]
    public class Scheduling
    {
        
             [Key]
            public int ID { get; set; }

            public int TrainerID { get; set; }

            public int MemberID { get; set; }

            public int? Status { get; set; }

            [StringLength(50)]
            public string MemName { get; set; }

            public string Description { get; set; }

            public int? Label { get; set; }

            public DateTime? StartTime { get; set; }

            public DateTime? EndTime { get; set; }

           

            public bool AllDay { get; set; }

            public int? EventType { get; set; }

            public string RecurrenceInfo { get; set; }

            public string ReminderInfo { get; set; }

        public virtual Member Member { get; set; }

        public virtual Trainer Trainer { get; set; }
    }
    }

