﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for CheckInPop.xaml
    /// </summary>
    public partial class CheckInPop : DevExpress.Xpf.Core.ThemedWindow
    {
        private Member currMember;
        public CheckInPop(Window parent)
        {
            InitializeComponent();
            Owner = parent;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (tbMember.Text == "")
            {
                MessageBox.Show("Please Enter a MemberID to Continue");
                return;
            }

            Globals.memId = int.Parse(tbMember.Text);
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    var mem = (from x in ctx.Members where x.MemberID == Globals.memId select x).ToList();
                    currMember = mem[0];
                    if (mem.Count == 0)
                    {
                        lblError.Content = "No member found with ID : " + Globals.memId;
                        lblError.Visibility = Visibility.Visible;
                        return;
                    }
                    currMember = mem[0];
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error getting MemberID:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);     
                
            }    
                CheckInOut check = new CheckInOut(this, currMember);
                check.ShowDialog();
                this.Visibility = Visibility.Hidden;
            
        }
    }
}
