﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : DevExpress.Xpf.Core.ThemedWindow
    {
        public MainMenu(Window parent)
        {
            InitializeComponent();
            Owner = parent;
           
            lblName.Content = Globals.loginFName + " " + Globals.loginLName;
            if (Globals.jobPosition == "Staff")
            {             

                btnSendEmail.IsEnabled = false;
                btnSendEmail.Opacity = .50;
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Email em = new Email(this);
            em.ShowDialog();
        }

        private void btnReports_Click(object sender, RoutedEventArgs e)
        {
            Reports rep = new Reports(this);
            rep.ShowDialog();
        }

        private void btnSession_Click(object sender, RoutedEventArgs e)
        {
            Reservation res = new Reservation(this);
            res.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AddMember addMem = new AddMember(this);
            addMem.ShowDialog();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            RenewMemPop renPop = new RenewMemPop(this);
            renPop.ShowDialog();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Shop2 shop = new Shop2(this);
            shop.ShowDialog();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to sign out? ", "Sing-out...", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.OK)
            {
                this.Visibility = Visibility.Hidden;
                MainWindow main = new MainWindow();
                main.ShowDialog();
            }      
                   
        }

        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            CheckInPop cip = new CheckInPop(this);
            cip.ShowDialog();
        }
      
    }
}
