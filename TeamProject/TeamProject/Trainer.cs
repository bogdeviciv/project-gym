namespace TeamProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Trainer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Trainer()
        {
            Sessions = new HashSet<Session>();
        }

        public int TrainerID { get; set; }

        [Required]
        [StringLength(50)]
        public string TRFirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string TRLastName { get; set; }

        [Required]
        [StringLength(50)]
        public string TRSpecialty { get; set; }

        [Required]
        [StringLength(50)]
        public string TRPhoneNumber { get; set; }

        [Column(TypeName = "image")]
        public byte[] TRPhoto { get; set; }

        [Required]
        [StringLength(50)]
        public string TREmail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session> Sessions { get; set; }
    }
}
