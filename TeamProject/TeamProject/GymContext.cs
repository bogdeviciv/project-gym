namespace TeamProject
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class GymContext : DbContext
    {
        public GymContext()
            : base("name=GymContext")
        {
        }

        public virtual DbSet<CheckIn> CheckIns { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<Membership> Memberships { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }
        public virtual DbSet<Trainer> Trainers { get; set; }


        
        public virtual DbSet<Scheduling> Schedulings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Sales)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Member>()
                .HasMany(e => e.CheckIns)
                .WithRequired(e => e.Member)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Member>()
                .HasMany(e => e.Sessions)
                .WithRequired(e => e.Member)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Member>()
                .HasMany(e => e.Sales)
                .WithRequired(e => e.Member)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Membership>()
                .Property(e => e.MemShipPrice)
                .HasPrecision(10, 4);

            modelBuilder.Entity<Membership>()
                .Property(e => e.MemShipDiscount)
                .HasPrecision(3, 0);

            modelBuilder.Entity<Membership>()
                .HasMany(e => e.Members)
                .WithRequired(e => e.Membership)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.ProductPrice)
                .HasPrecision(10, 4);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.Sales)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sale>()
                .Property(e => e.UnitPrice)
                .HasPrecision(10, 4);

            modelBuilder.Entity<Trainer>()
                .HasMany(e => e.Sessions)
                .WithRequired(e => e.Trainer)
                .WillCascadeOnDelete(false);
        }
    }
}
