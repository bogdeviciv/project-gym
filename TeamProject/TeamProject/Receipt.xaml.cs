﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for Receipt.xaml
    /// </summary>
    public partial class Receipt : Window
    {
        public Receipt(Window parent)
        {
            InitializeComponent();
            Owner = parent;
            lvItems.ItemsSource = Globals.cart;
            lblTotal.Content = "$"+Globals.TotalCost;
            DateTime localDate = DateTime.Now;
            lblDate.Content = localDate.ToString("MM/dd/yyyy h:mm tt");
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            
            PrintDialog dlg = new PrintDialog();           
            btnPrint.Visibility = Visibility.Hidden;
            dlg.PrintVisual(this, "Window Printing.");
        }


    }
}
