﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for CheckInOut.xaml
    /// </summary>
    public partial class CheckInOut : DevExpress.Xpf.Core.ThemedWindow
    {
        private Member currMember;
        public DateTime localDate = DateTime.Now;
        public CheckInOut(Window parent, Member _currMember = null)
        {
            InitializeComponent();
            Owner = parent;
            currMember = _currMember;

            string[] split = currMember.MemName.Split(null);
            tbFirstName.Content = split[0];
            tbLastName.Content = split[1];
            tbAddress.Content = currMember.MemAddress;

            try
            {
                using (GymContext ctx = new GymContext())
                {
                    var memberx = ctx.Members.FirstOrDefault(p => p.MemberID == Globals.memId);
                    var memType = ctx.Memberships.FirstOrDefault(p => p.MemShipID == memberx.MemShipID);

                    lblMemberType.Content = memType.MemShipType.ToString();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error getting Member Types:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


            lblBirthdate.Content = currMember.MemDateofBirth.ToString("yyyy/MM/dd");
            tbPhoneNum.Content = currMember.MemPhoneNumber;
            if (currMember.MemPhoto != null)
            {
                imgMem.Source = ByteToImage(currMember.MemPhoto);
            }
            tbEmail.Content = currMember.MemEmail;
        }

        private void btnCheckOut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    var checkMem = ctx.CheckIns.FirstOrDefault(p => p.MemberID == Globals.memId);
                    CheckIn z = checkMem;
                    z.CheckOutDateTime = localDate;
                    ctx.SaveChanges();

                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error adding check out:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            this.Visibility = Visibility.Hidden;
        }

        private void btnCheckIn_Click(object sender, RoutedEventArgs e)
        {

            CheckIn x = new CheckIn()
            {
                MemberID = Globals.memId,
                CheckInDateTime = localDate,
            };
            try
            {


                using (GymContext ctx = new GymContext())
                {
                    ctx.CheckIns.Add(x);
                    ctx.SaveChanges();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error adding check in:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            this.Visibility = Visibility.Hidden;
        }

        public static ImageSource ByteToImage(byte[] imageData)
        {

            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();
            ImageSource imgSrc = biImg as ImageSource;
            return imgSrc;
        }
    }
}
