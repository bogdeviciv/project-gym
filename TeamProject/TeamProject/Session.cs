namespace TeamProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Session
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Session()
        {
            CheckIns = new HashSet<CheckIn>();
        }

        [Key]
        public int SessionsID { get; set; }

        public int TrainerID { get; set; }

        public int MemberID { get; set; }

        [Required]
        [StringLength(50)]
        public string SessionType { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime SessionStartDate { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime SessionEndDate { get; set; }

        public int? Label { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CheckIn> CheckIns { get; set; }

        public virtual Member Member { get; set; }

        public virtual Trainer Trainer { get; set; }
    }
}
