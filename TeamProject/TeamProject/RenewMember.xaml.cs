﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing.Imaging;
using System.Data.SqlClient;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for RenewMember.xaml
    /// </summary>
    public partial class RenewMember : DevExpress.Xpf.Core.ThemedWindow
    {
      
        private Member currMember;
        public RenewMember(Window parent, Member _currMember = null)
        {
            InitializeComponent();
            Owner = parent;
            currMember = _currMember;

            try
            {
                using (GymContext ctx = new GymContext())
                {
                    var MembershipTypes = (from x in ctx.Memberships select x.MemShipType).ToList();

                    cbMemType.ItemsSource = MembershipTypes;
                }
                if (currMember != null)
                {
                    string[] split = currMember.MemName.Split(null);
                    tbFirstName.Text = split[0];
                    tbLastName.Text = split[1];
                    tbAddress.Text = currMember.MemAddress;

                    cbMemType.SelectedIndex = currMember.MemShipID - 1;

                    birthDatePicker.Text = currMember.MemDateofBirth.ToString("yyyy/MM/dd");
                    tbPhoneNum.Text = currMember.MemPhoneNumber;
                    if (currMember.MemPhoto != null)
                    {
                        imgMem.Source = ByteToImage(currMember.MemPhoto);
                    }
                    tbEmail.Text = currMember.MemEmail;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error retrieving data:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnRenewMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    var member = (from p in ctx.Members where p.MemberID == Globals.memId select p).ToList();
                    Member m = member[0];
                    m.MemExpiryDate = m.MemExpiryDate.AddMonths(int.Parse(cbNumMonths.Text));
                    ctx.SaveChanges();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error retrieving data:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            this.Visibility = Visibility.Hidden;
        }

      
            public static ImageSource ByteToImage(byte[] imageData)
            {
          
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();
                ImageSource imgSrc = biImg as ImageSource;
                return imgSrc;
            }
        

    }
}
