﻿using System;
using System.IO;
using System.Windows;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Threading;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for Email.xaml
    /// </summary>
    public partial class Email : DevExpress.Xpf.Core.ThemedWindow
    {
        public DateTime localDate = DateTime.Now;
        public StringBuilder allEmail = new StringBuilder();
        public StringBuilder memEmail = new StringBuilder();
        public Email(Window parent)
        {
            InitializeComponent();
            Owner = parent;
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    //All Members
                    var memExp = (from x in ctx.Members select x).ToList();
                    foreach (var mem in memExp)
                    {
                        allEmail.Append(mem.MemEmail + ",");
                    }
                    //Trainers
                    var trainers = (from x in ctx.Trainers select x).ToList();
                    foreach (var mem in trainers)
                    {
                        allEmail.Append(mem.TREmail + ",");
                    }
                    //Employees
                    var emps = (from x in ctx.Employees select x).ToList();
                    foreach (var mem in emps)
                    {
                        allEmail.Append(mem.EmpEmail + ",");
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error retrieving data:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void rbMembers_Checked(object sender, RoutedEventArgs e)
        {
            cbMembers.Visibility = Visibility.Visible;
            lblMem.Visibility = Visibility.Visible;

        }





        //using gmail scope
        static string[] Scopes = { GmailService.Scope.GmailSend };
        static string ApplicationName = "SendEmail";

        private void btSendClick(object sender, RoutedEventArgs e)
        {
            UserCredential credential;
            //read credentials file
            using (FileStream stream = new FileStream(System.AppDomain.CurrentDomain.BaseDirectory + @"/credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                credPath = System.IO.Path.Combine(credPath, ".credentials/gmail-dotnet-quickstart.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync
                (GoogleClientSecrets.Load(stream).Secrets, Scopes, "user", CancellationToken.None, new FileDataStore(credPath, true)).Result;
            }

            string plainText = $"To: {txtTo.Text}\r\n" +
                               $"Subject: {txtSubject.Text}\r\n" +
                               "Content-Type: text/html; charset=utf-8\r\n\r\n" +
                               $"<h6>{txtMessage.Text}</h6>";

            //call gmail service
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            var newMsg = new Google.Apis.Gmail.v1.Data.Message();
            newMsg.Raw = Base64UrlEncode(plainText.ToString());
            service.Users.Messages.Send(newMsg, "me").Execute();
            MessageBox.Show("Your email has been successfully sent !", "Message");
        }


        public static string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(inputBytes).Replace("+", "-").Replace("/", "_").Replace("=", "");
        }

        private void rbEmp_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                using (GymContext ctx = new GymContext())
                {

                    var emps = (from x in ctx.Employees select x).ToList();
                    foreach (var mem in emps)
                    {
                        memEmail.Append(mem.EmpEmail + ",");
                    }
                    txtTo.Text = memEmail.ToString();
                    memEmail.Clear();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error retrieving data:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void rbTrainers_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                using (GymContext ctx = new GymContext())
                {

                    var emps = (from x in ctx.Trainers select x).ToList();
                    foreach (var mem in emps)
                    {
                        memEmail.Append(mem.TREmail + ",");
                    }
                    txtTo.Text = memEmail.ToString();
                    memEmail.Clear();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error retrieving data:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void rbAll_Checked(object sender, RoutedEventArgs e)
        {
            txtTo.Text = allEmail.ToString();
        }

        private void cbMembers_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    if (cbMembers.SelectedIndex == 0)
                    {
                        var memExp = (from x in ctx.Members where (x.MemExpiryDate.Month == localDate.Month) select x).ToList();
                        foreach (var mem in memExp)
                        {
                            memEmail.Append(mem.MemEmail + ",");

                        }
                        txtTo.Text = memEmail.ToString();
                        memEmail.Clear();
                    }
                    else if (cbMembers.SelectedIndex == 1)
                    {
                        var memSched = (from zx in ctx.Schedulings join xc in ctx.Members on zx.MemberID equals xc.MemberID select xc.MemEmail).ToList();
                        foreach (var mem in memSched)
                        {
                         memEmail.Append(mem + ",");
                        }
                        txtTo.Text = memEmail.ToString();
                        memEmail.Clear();
                    }
                    else if (cbMembers.SelectedIndex == 2)
                    {
                        var memExp = (from x in ctx.Members select x).ToList();
                        foreach (var mem in memExp)
                        {
                            memEmail.Append(mem.MemEmail + ",");

                        }
                        txtTo.Text = memEmail.ToString();
                        memEmail.Clear();
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error retrieving data:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void rbOther_Checked(object sender, RoutedEventArgs e)
        {
            txtTo.Text = "";
        }
    }
}
