namespace TeamProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Membership
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Membership()
        {
            Members = new HashSet<Member>();
        }

        [Key]
        public int MemShipID { get; set; }

        [Required]
        [StringLength(50)]
        public string MemShipType { get; set; }

        [StringLength(50)]
        public string MemShipNotes { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal MemShipPrice { get; set; }

        public decimal? MemShipDiscount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Member> Members { get; set; }
    }
}
