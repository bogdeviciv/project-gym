﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for RegisterEmpPop.xaml
    /// </summary>
    public partial class RegisterEmpPop : DevExpress.Xpf.Core.ThemedWindow
    {
        public RegisterEmpPop(Window parent)
        {
            InitializeComponent();
            Owner = parent;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {           
            
            if(tbCode.Text != "Reg123")
            {
                MessageBox.Show("Invalid Code Given");
                tbCode.Text = "";
                return;
            }

            AddEmployee addEmp = new AddEmployee(this);
            addEmp.ShowDialog();
            this.Visibility = Visibility.Hidden;
        }
    }
}
