﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for Reservation.xaml
    /// </summary>
    public partial class Reservation : DevExpress.Xpf.Core.ThemedWindow
    {
        public Reservation(Window parent)
        {
            InitializeComponent();
            Owner = parent;
            DateTime todays = DateTime.Now;
            lblDateTime.Content = todays.ToLongDateString() + " " + todays.ToShortTimeString();
           
        }

      



    }
}
