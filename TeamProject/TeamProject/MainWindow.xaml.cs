﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : DevExpress.Xpf.Core.ThemedWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            // loadImages();
            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource =
                new BitmapImage(new Uri(@"..\..\cover.png", UriKind.Relative));
            this.Background = myBrush;
            this.Background.Opacity = 0.5;


        }


        public void loadImages()
        {
            List<String> paths = new List<string>
            {
                "D:\\gymbag.jpg",
                "D:\\shakercup.jpg",
                "D:\\gymtowel.jpg",
                "D:\\waterbottle.jpg",
                "D:\\staintless.jpg",
                "D:\\awheypowder.png",
                "D:\\creatine.jpg",
                "D:\\amyno.jpg",
                "D:\\glutamine.jpg"
            };
            try
            {
                using (GymContext ctx = new GymContext())
                {
                    var products = (from p in ctx.Products select p).ToList();
                    for (int i = 0; i < 9; i++)
                    {
                        BitmapImage imageC = new BitmapImage(new Uri(paths[i]));
                        MemoryStream memStream = new MemoryStream();
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(imageC));
                        encoder.Save(memStream);
                        byte[] imageArray = memStream.ToArray();
                        products[i].ProductImage = imageArray;
                        ctx.SaveChanges();
                    }

                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error adding images:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }





        private void btnSub_Click(object sender, RoutedEventArgs e)
        {
                    
            if(tbUserName.Text == "admin" && pbPassword.Password == "admin")
            {
                Globals.loginFName = "Admin";
                Globals.loginLName = "";
                Globals.jobPosition = "Admin";
                MainMenu xx = new MainMenu(this);
                this.Visibility = Visibility.Hidden;
                xx.ShowDialog();
               

            }
            else
            {
                try
                {
                    using (GymContext ctx = new GymContext())
                    {
                        var UserNames = (from x in ctx.Employees where x.EmpUser == tbUserName.Text select x).ToList();
                        if (UserNames.Count == 1)
                        {
                            var Password = (from x in ctx.Employees where x.EmpPassword == pbPassword.Password select x).ToList();
                            if (Password.Count == 0)
                            {
                                MessageBox.Show("User Name or Password is incorrect");
                            }
                            else
                            {
                                Globals.loginFName = UserNames[0].EmpFirstName;
                                Globals.loginLName = UserNames[0].EmpLastName;
                                Globals.jobPosition = UserNames[0].EmpTitle;
                                Globals.empId = UserNames[0].EmpID;

                                MainMenu xx = new MainMenu(this);
                                this.Visibility = Visibility.Hidden;
                                xx.ShowDialog();

                            }
                        }
                        else
                        {
                            MessageBox.Show("User Name or Password is incorrect");
                        }

                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(this, "Error retrieving data:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
          
            
            //MainMenu mm = new MainMenu(this);
            //mm.ShowDialog();
        }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {
            RegisterEmpPop dlg = new RegisterEmpPop(this);
            dlg.ShowDialog();
        }
    }
}
