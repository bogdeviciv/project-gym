﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SendEmailTestProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       
        public MainWindow()
        {

            InitializeComponent();
           
        }

        //using gmail scope
        static string[] Scopes = { GmailService.Scope.GmailSend };
        static string ApplicationName = "SendEmailTestProject";




        private void Button_Click(object sender, RoutedEventArgs e)
        {
            UserCredential credential;
            //read credentials file
            using (FileStream stream = new FileStream(System.AppDomain.CurrentDomain.BaseDirectory + @"/credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                credPath = System.IO.Path.Combine(credPath, ".credentials/gmail-dotnet-quickstart.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets, Scopes, "user", CancellationToken.None, new FileDataStore(credPath, true)).Result;
            }

            string plainText = $"To: {txtTo.Text}\r\n" +
                               $"Subject: {txtSubject.Text}\r\n" +
                               "Content-Type: text/html; charset=utf-8\r\n\r\n" +
                               $"<h1>{txtMessage.Text}</h1>";

            //call gmail service
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            var newMsg = new Google.Apis.Gmail.v1.Data.Message();
            newMsg.Raw = Base64UrlEncode(plainText.ToString());
            service.Users.Messages.Send(newMsg, "me").Execute();
            MessageBox.Show("Your email has been successfully sent !", "Message");
        }
    

        public static string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(inputBytes).Replace("+", "-").Replace("/", "_").Replace("=", "");
        }


    }
}
