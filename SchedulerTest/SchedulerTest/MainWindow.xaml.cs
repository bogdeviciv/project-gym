﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchedulerTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        gymDataSet dataSet;
        gymDataSetTableAdapters.AppointmentsTableAdapter adapter;
        gymDataSetTableAdapters.ResourcesTableAdapter resourcesAdapter;

        public MainWindow()
        {
            InitializeComponent();
            this.dataSet = new gymDataSet();

            // Bind the scheduler storage to appointment data.    
            this.schedulerControl1.Storage.AppointmentStorage.DataSource = dataSet.Appointments;
            

            // Load data to the 'SchedulerTestDataSet.Appointments' table.     
            this.adapter = new gymDataSetTableAdapters.AppointmentsTableAdapter();
            this.adapter.Fill(dataSet.Appointments);

            // Bind the scheduler storage to resource data.    
            this.schedulerControl1.Storage.ResourceStorage.DataSource = dataSet.Resources;

            // Load data to the 'ShedulerBindingTestDataSet.Resources' table.     
            this.resourcesAdapter = new gymDataSetTableAdapters.ResourcesTableAdapter();
            resourcesAdapter.Fill(dataSet.Resources);

            this.schedulerControl1.Storage.AppointmentsInserted +=
                new PersistentObjectsEventHandler(Storage_AppointmentsModified);
            this.schedulerControl1.Storage.AppointmentsChanged +=
                new PersistentObjectsEventHandler(Storage_AppointmentsModified);
            this.schedulerControl1.Storage.AppointmentsDeleted +=
                new PersistentObjectsEventHandler(Storage_AppointmentsModified);
        }

        void Storage_AppointmentsModified(object sender, PersistentObjectsEventArgs e)
        {
            this.adapter.Adapter.Update(this.dataSet);
            this.dataSet.AcceptChanges();
        }
    }
}
