﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPeople
{
    class Person
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50),Required]
        //[Index(IsUnique=true)]
        public string Name { get; set; }
        [Required]
        public int Age { get; set; }
        [Required]
        public GenderEnum Gender { get; set; }
        public enum GenderEnum { Male=1,Female=2,NA=0 }

        public override string ToString()
        {
           return String.Format("id {0} name {1} , age {2}, gender {3}", Id, Name, Age, Gender);
        }
    }
}
