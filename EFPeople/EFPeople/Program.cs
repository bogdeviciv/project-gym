﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPeople
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            using (SocietyContext stx = new SocietyContext())
            {

                Person p = new Person()
                {
                    Name = "Jerry",
                    Age = rnd.Next(1, 100),
                    Gender= Person.GenderEnum.Male
                };
                stx.People.Add(p);
                stx.SaveChanges();


            }

            using (SocietyContext stx = new SocietyContext())
            {

                var people = (from p in stx.People where p.Id == 2 select p).ToList();
                if (people.Count == 0) Console.WriteLine("Record not found");
                else
                {
                    Person p = people[0];
                    Console.WriteLine("Person found "+p.ToString());
                    p.Age = 99;
                    p.Gender = Person.GenderEnum.Male;
                    stx.SaveChanges();
                    stx.People.Remove(p);
                    stx.SaveChanges();
                }
                Console.ReadKey();


            }

            using (SocietyContext stx = new SocietyContext())
            {

                var people = (from p in stx.People where p.Id == 4 select p).ToList();
                if (people.Count == 0) Console.WriteLine("Record not found");
                else
                {
                    Person p = people[0];
                    Console.WriteLine("Person found " + p.ToString());                                      
                   stx.People.Remove(p); //scheduled for deletion
                    stx.SaveChanges();
                }
                Console.ReadKey();


            }
        }
    }
}
