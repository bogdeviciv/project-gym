﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace PrintTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog dlg = new PrintDialog();
            btnPrint.Visibility = Visibility.Hidden;
            dlg.PrintVisual(gridMain, "Window Printing.");
        }
    }
}

